class UsersController < ApplicationController
  def index
	@user = User.all
	render :json => {item: @user}
	
  end
  
  def show
	@user = User.find_by_id(params[:id])
	
	if @user.presence
		render :json => {item: @user,message:"Success",status:200}
	else
		render :json => {item: @user,message:"Do not found any datas!",status:402}
	end
	
  end
  
end
