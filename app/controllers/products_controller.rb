class ProductsController < ApplicationController
  def index
	@product = Product.all
	render :json => {item: @product}
	
  end
  
  def show
	@product = Product.find_by_id(params[:id])
	
	if @product.presence
		render :json => {item: @product,message:"Success",status:200}
	else
		render :json => {item: @product,message:"Do not found any datas!",status:402}
	end
	
  end
  
end
