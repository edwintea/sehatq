Rails.application.routes.draw do
  get 'carts/index'
  get 'products/index'
  get 'users' => 'users#index'
  get 'users/:id' => 'users#show'
  get 'products' => 'products#index'
  get 'products/:id' => 'products#show'
  get 'welcome/index'
  
  root 'welcome#index'
end
